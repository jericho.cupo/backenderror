package com.example.eCommerce.Model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

import static javax.persistence.CascadeType.REMOVE;

@Data
@Entity
@Table(name = "userCard")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class userCard implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int cardId;
    @Column
    private String cardName;
    @Column
    private long cardNumber;
    @Column
    private int userAccount_userId;
    @Column
    private Double cardBalance;
    @Column
    private Double cardLimit;
    @Column
    private String cardType;

    @OneToMany(fetch = FetchType.LAZY, orphanRemoval = false, cascade = REMOVE, mappedBy = "userCards_cardId")
    private List<checkOut> checkOuts;
}
