package com.example.eCommerce.Services.cards;

import com.example.eCommerce.Model.userCard;

public interface userCardsService {

    userCard addCard(userCard newCard);

    userCard findById(int id);
}
