package com.example.eCommerce.Model.ResponseBody;


import lombok.Data;

@Data
public class checkOutResBody {

    private String message;
    private Object payload;

    public checkOutResBody(String message, Object payload) {
        this.message = message;
        this.payload = payload;
    }
}
