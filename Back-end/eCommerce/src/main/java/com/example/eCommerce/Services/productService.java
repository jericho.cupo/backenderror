package com.example.eCommerce.Services;

import com.example.eCommerce.Model.RequestBody.productReqBody;
import com.example.eCommerce.Model.product;
import com.example.eCommerce.Repository.productRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class productService {

    @Autowired
    private productRepo repo;

    //All Products
    public List<product> listAll(){
        return repo.findAll();
    }

    //All available products
    public List<product> allAvailable(){return repo.selectProdAvailable();}

    //Product with 0 stocks
    public  List<product> noStocks(){return repo.selectProdNoStock();}


    //Adding Products
    public product save(productReqBody proReqBody) {

        product pro = new product();
        pro.setProductName(proReqBody.getProductName());
        pro.setProductPrize(proReqBody.getProductPrize());
        pro.setProductQuantity(proReqBody.getProductQuantity());
        pro.setProductImage(proReqBody.getProductImage());
        pro.setProductBrand(proReqBody.getProductBrand());
        pro.setProductDescription(proReqBody.getProductDescription());

        return repo.save(pro);
    }

    //Find Product by ID
    public product findById(Integer id){
        final var prod = repo.findById(id).get();
        return prod;
    }

    //Find by keyword in product name
    public List<product> getByKeyword(String keyWord) {
        return repo.selectByKeyword(keyWord);
    }

    //Updating Products
    public product updateProduct(product prod) {
        product pro = repo.getById(prod.getProductId());
        pro.setProductName(prod.getProductName());
        pro.setProductPrize(prod.getProductPrize());
        pro.setProductQuantity(prod.getProductQuantity());
        pro.setProductImage(prod.getProductImage());
        pro.setProductBrand(prod.getProductBrand());
        pro.setProductDescription(prod.getProductDescription());
        return repo.save(pro);
    }

    public product update(product prod){
        return repo.save(prod);
    }
}
