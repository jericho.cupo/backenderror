package com.example.eCommerce.Model;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import static javax.persistence.CascadeType.REMOVE;

@Data
@Entity
@Table(name = "userAccount")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class userAccount implements UserDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column()
    private int userId;
    @Column()
    private String firstName;
    @Column()
    private String middleName;
    @Column()
    private String lastName;
    @Column()
    private String gender;
    @Column()
    private String contactNumber;
    @Column()
    private String address;
    @Column()
    private String username;
    @Column()
    private String password;
    @Column()
    private String accountType;

    @OneToMany(fetch = FetchType.LAZY, orphanRemoval = false, cascade = REMOVE, mappedBy = "userAccount_userId")
    private List<userCard> userCards;

    @OneToMany(fetch = FetchType.LAZY, orphanRemoval = false, cascade = REMOVE, mappedBy = "userAccount_userId")
    private List<checkOut> checkOuts;

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name = "userAuthority", joinColumns = @JoinColumn(referencedColumnName = "userid"),inverseJoinColumns = @JoinColumn(referencedColumnName = "authorityid"))
    private List<Authority> authorities;


    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public String getUsername() {
        return this.username;
    }
}
