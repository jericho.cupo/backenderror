package com.example.eCommerce;

import com.example.eCommerce.Model.Authority;
import com.example.eCommerce.Model.userAccount;
import com.example.eCommerce.Repository.userRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class ECommerceApplication {

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private userRepo userDetailsRepository;

	public static void main(String[] args) {
		SpringApplication.run(ECommerceApplication.class, args);
	}

	@PostConstruct
	protected void init() {
		List<Authority> authorityList = new ArrayList<>();

		authorityList.add(createAuthority("USER", "user role"));
		authorityList.add(createAuthority("ADMIN", "admin role"));

		userAccount user = new userAccount();
		user.setUsername("admin1");
		user.setFirstName("Jericho");
		user.setLastName("Cupo");
		user.setPassword(passwordEncoder.encode("12345"));
		user.setAuthorities(authorityList);

		userDetailsRepository.save(user);

	}

	private Authority createAuthority(String roleCode, String roleDescription) {
		Authority authority = new Authority();
		authority.setRoleCode(roleCode);
		authority.setRoleDescription(roleDescription);
		return authority;
	}


}
