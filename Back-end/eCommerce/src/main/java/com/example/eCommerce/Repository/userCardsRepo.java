package com.example.eCommerce.Repository;

import com.example.eCommerce.Model.userCard;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.CrossOrigin;

@Repository
public interface userCardsRepo extends JpaRepository<userCard, Integer> {

}
