package com.example.eCommerce.Repository;

import com.example.eCommerce.Model.cart;
import com.example.eCommerce.Model.cartHasProducts;
import org.springframework.data.jpa.repository.JpaRepository;

public interface cartHasProductsRepo extends JpaRepository<cartHasProducts, Integer> {
}
