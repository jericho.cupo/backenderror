package com.example.eCommerce.Model.ResponseBody;


import lombok.Data;

@Data
public class productResBody {
    private String message;
    private Object payload;

    public productResBody(String message, Object payload) {
        this.message = message;
        this.payload = payload;
    }
}
