package com.example.eCommerce.Controller;

import com.example.eCommerce.Model.RequestBody.cartHasProductsReqBody;
import com.example.eCommerce.Model.RequestBody.cartReqBody;
import com.example.eCommerce.Model.ResponseBody.cartHasProductResBody;
import com.example.eCommerce.Model.ResponseBody.cartResBody;
import com.example.eCommerce.Model.cart;
import com.example.eCommerce.Model.cartHasProducts;
import com.example.eCommerce.Services.cartHasProductsService;
import com.example.eCommerce.Services.cartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class cartHasProductsController {
    @Autowired
    private cartHasProductsService cart_service;

    //All products in Cart
    @GetMapping("/cartProducts")
    public List<cartHasProducts> list(){
        return cart_service.listAll();
    }

    //adding products in user's cart
    @PostMapping("/cartProducts/cartHasProducts")
    public ResponseEntity<cartHasProductResBody> saveCart(@RequestBody cartHasProductsReqBody cart1){
        cartHasProducts cartHasProd = cart_service.save(cart1);
        return new ResponseEntity<cartHasProductResBody>(new cartHasProductResBody("Successfully added product in cart.",cartHasProd), HttpStatus.OK);
    }

}
