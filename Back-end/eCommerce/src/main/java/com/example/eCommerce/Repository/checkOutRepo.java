package com.example.eCommerce.Repository;

import com.example.eCommerce.Model.cart;
import com.example.eCommerce.Model.checkOut;
import org.springframework.data.jpa.repository.JpaRepository;

public interface checkOutRepo extends JpaRepository<checkOut, Integer> {
}
