package com.example.eCommerce.Model.RequestBody;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class cartHasProductsReqBody {

    private int cart_has_productsId;
    private int cart_cartId;
    private int products_productId;
}
