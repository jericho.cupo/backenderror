package com.example.eCommerce.Model.RequestBody;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class productReqBody {


    private int productId;
    private String productName;
    private Double productPrize;
    private int productQuantity;
    private String productImage;
    private String productBrand;
    private String productDescription;
}
