package com.example.eCommerce.Controller;

import com.example.eCommerce.Model.RequestBody.cartReqBody;
import com.example.eCommerce.Model.RequestBody.checkOutReqBody;
import com.example.eCommerce.Model.ResponseBody.cartResBody;
import com.example.eCommerce.Model.ResponseBody.checkOutResBody;
import com.example.eCommerce.Model.cart;
import com.example.eCommerce.Model.checkOut;
import com.example.eCommerce.Services.cartService;
import com.example.eCommerce.Services.checkOutService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class checkOutController {

    @Autowired
    private checkOutService checkOut_service;

    //All Carts
    @GetMapping("/checkOut")
    public List<checkOut> list(){
        return checkOut_service.listAll();
    }


    //adding cart for user
    @PostMapping("/checkOut/addCheckOut")
    public ResponseEntity<checkOutResBody> saveCart(@RequestBody checkOutReqBody checkOutReq){
        checkOut check_out = checkOut_service.save(checkOutReq);
        return new ResponseEntity<checkOutResBody>(new checkOutResBody("Successfully added checkout.",check_out), HttpStatus.OK);
    }
}
