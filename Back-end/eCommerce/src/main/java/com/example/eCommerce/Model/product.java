package com.example.eCommerce.Model;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

import static javax.persistence.CascadeType.REMOVE;


@Data
@Entity
@Table(name = "product")
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Getter
@Setter
public class product implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int productId;
    @Column
    private String productName;
    @Column
    private Double productPrize;
    @Column
    private int productQuantity;
    @Column
    private String productImage;
    @Column
    private String productBrand;
    @Column()
    private String productDescription;

    @OneToMany(fetch = FetchType.LAZY, orphanRemoval = false, cascade = REMOVE, mappedBy = "products_productId")
    private List<checkOutProducts> checkOutProducts;

    @OneToMany(fetch = FetchType.LAZY, orphanRemoval = false, cascade = REMOVE, mappedBy = "products_productId")
    private List<cartHasProducts> cartProducts;

}
