package com.example.eCommerce.SecurityController;

import com.example.eCommerce.Config.JWTTokenHelper;
import com.example.eCommerce.Model.userAccount;
import com.example.eCommerce.SecurityRequest.AuthenticationRequest;
import com.example.eCommerce.SecurityResponses.LoginResponse;
import com.example.eCommerce.SecurityResponses.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.*;

import java.security.NoSuchAlgorithmException;
import java.security.Principal;
import java.security.spec.InvalidKeySpecException;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class AuthenticationController {
    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    JWTTokenHelper jwtTokenHelper;

    @Autowired
    private UserDetailsService userDetailsService;



    @PostMapping("/auth/login")
    public ResponseEntity<?> login(@RequestBody AuthenticationRequest authenticationRequest) throws InvalidKeySpecException, NoSuchAlgorithmException {

        final Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(authenticationRequest.getUserName(),
                        authenticationRequest.getPassword())
        );


        SecurityContextHolder.getContext().setAuthentication(authentication);

        userAccount user =(userAccount) authentication.getPrincipal();
        String jwtToken = jwtTokenHelper.generateToken(user.getUsername());

        LoginResponse response = new LoginResponse();
        response.setToken(jwtToken);


        return ResponseEntity.ok(response);
    }

    @GetMapping("/auth/userInfo")
    public ResponseEntity<?>getUserInfo(Principal user){
        userAccount userObj=(userAccount) userDetailsService.loadUserByUsername(user.getName());

        UserInfo userInfo = new UserInfo();
        userInfo.setFirstname(userObj.getFirstName());
        userInfo.setLastname(userObj.getLastName());
        userInfo.setUsername(userObj.getUsername());
        userInfo.setAccountType(userObj.getAccountType());


        return ResponseEntity.ok(userInfo);
    }
}
