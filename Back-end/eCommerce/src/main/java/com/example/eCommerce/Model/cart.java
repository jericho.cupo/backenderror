package com.example.eCommerce.Model;


import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

import static javax.persistence.CascadeType.REMOVE;

@Data
@Entity
@Table(name = "cart")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class cart implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column()
    private int cartId;
    @Column
    private int orderQuantity;
//    @Column
//    private int userAccount_userId;

    @OneToMany(fetch = FetchType.LAZY, orphanRemoval = false, cascade = REMOVE, mappedBy = "cart_cartId")
    private List<cartHasProducts> cartProducts;

    @OneToMany(fetch = FetchType.LAZY, orphanRemoval = false, cascade = REMOVE, mappedBy = "cart_cartId")
    private List<checkOut> checkOuts;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "Account_userId", referencedColumnName = "userId")
    private userAccount account;

}
