package com.example.eCommerce.Services;


import com.example.eCommerce.Model.RequestBody.cartHasProductsReqBody;
import com.example.eCommerce.Model.RequestBody.cartReqBody;
import com.example.eCommerce.Model.cart;
import com.example.eCommerce.Model.cartHasProducts;
import com.example.eCommerce.Repository.cartHasProductsRepo;
import com.example.eCommerce.Repository.cartRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class cartHasProductsService {
    @Autowired
    private cartHasProductsRepo repo;

    //All products in Cart
    public List<cartHasProducts> listAll(){
        return repo.findAll();
    }

    public cartHasProducts save(cartHasProductsReqBody cartReqBody) {
        cartHasProducts cartProducts = new cartHasProducts();
        cartProducts.setCart_cartId(cartReqBody.getCart_cartId());
        cartProducts.setProducts_productId(cartReqBody.getProducts_productId());
        return repo.save(cartProducts);
    }
}
