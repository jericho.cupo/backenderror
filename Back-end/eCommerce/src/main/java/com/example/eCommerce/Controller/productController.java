package com.example.eCommerce.Controller;


import com.example.eCommerce.Model.RequestBody.productReqBody;
import com.example.eCommerce.Model.ResponseBody.productResBody;
import com.example.eCommerce.Model.product;
import com.example.eCommerce.Services.productService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
public class productController {

    @Autowired
    private productService prod_service;

    //Return All Products
    @GetMapping("/productList")
    public List<product> list(){
        return prod_service.listAll();
    }

    //Return All Available Products
    @GetMapping ("/availableProducts")
    public List<product> productListAvailable(){
        return prod_service.allAvailable();
    }

    //Get products by id
    @GetMapping("/productList/product")
    public ResponseEntity<product> getProductById(@RequestParam int id){
        product prod = prod_service.findById(id);
        return ResponseEntity.ok(prod);
    }

    //Searching product by productName
    @GetMapping("/productList/prodName")
    public List<product> getFilteredList(@RequestParam String prodName){
        return prod_service.getByKeyword(prodName);
    }

    //Returning products with 0 stocks
    @GetMapping("/productList/noStocks")
    public List<product> productListNoStocks(){
        return prod_service.noStocks();
    }

    //Adding Products
    @PostMapping("/productList/addProduct")
    public ResponseEntity<productResBody> saveProduct(@RequestBody productReqBody pro) {
        try {
            product prod = prod_service.save(pro);
            return new ResponseEntity<productResBody>(new productResBody("Successfully save product.", prod), HttpStatus.OK);
        }catch(Exception e){
            return new ResponseEntity<productResBody>(new productResBody("An Error occurs in saving.", e), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Updating Product
    @PutMapping("/product/update")
    public productResBody updateProduct(@RequestBody product prod){
        prod = prod_service.updateProduct(prod);

        System.out.println("Product Info "+prod);
        return new productResBody("Updated Successfully",prod);
    }

    //Updating Product by ID
    @PutMapping("/product/update/{id}")
    public productResBody updateProductById(@PathVariable Integer id,@RequestBody product pro) {
        product prod = new product();
        prod.setProductId(id);
        prod.setProductName(pro.getProductName());
        prod.setProductPrize(pro.getProductPrize());
        prod.setProductQuantity(pro.getProductQuantity());
        prod.setProductImage(pro.getProductImage());
        prod.setProductBrand(pro.getProductBrand());
        prod.setProductDescription(pro.getProductDescription());

            prod_service.update(prod);
            return new productResBody("Record for id " + id + " has been updated Successfully", pro);

    }
}
