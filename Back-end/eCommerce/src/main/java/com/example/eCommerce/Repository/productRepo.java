package com.example.eCommerce.Repository;

import com.example.eCommerce.Model.product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface productRepo extends JpaRepository<product, Integer> {

    //all available
    @Query(value = "SELECT * FROM product p where p.product_quantity > '0' " ,nativeQuery = true)
    List<product> selectProdAvailable();

    //Search product by product name
    @Query(value = "SELECT * FROM product p where p.product_name like %:keyword% " ,nativeQuery = true)
    List<product> selectByKeyword(String keyword);

    @Query(value = "SELECT * FROM product p where p.product_quantity = '0' " ,nativeQuery = true)
    List<product> selectProdNoStock();

}
