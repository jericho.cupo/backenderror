package com.example.eCommerce.Services.user;

import com.example.eCommerce.Model.userAccount;
import com.example.eCommerce.Repository.userRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class userServiceImp implements userService{

    @Autowired
    private userRepo userRepo;

    @Override
    public userAccount addUser(userAccount user) {
        return userRepo.save(user);
    }

    @Override
    public List<userAccount> allUsers() {
        return userRepo.findAll();
    }

//    @Override
//    public List<userAccount> findByCreds(String username, String password) {
//        return userRepo.findByCreds(username, password);
//    }
}
