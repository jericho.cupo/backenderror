package com.example.eCommerce.Model.RequestBody;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import java.sql.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class checkOutReqBody {
    private int checkOutId;
    private Double checkOutAmount;
    private Date checkOutDate;
    private int userAccount_userId;
    private  int userCards_cardId;
    private  int cart_cartId;
}
