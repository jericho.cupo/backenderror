package com.example.eCommerce.Repository;

import com.example.eCommerce.Model.cart;
import org.springframework.data.jpa.repository.JpaRepository;

public interface cartRepo extends JpaRepository<cart, Integer> {
}
