package com.example.eCommerce.Services;

import com.example.eCommerce.Model.RequestBody.cartReqBody;
import com.example.eCommerce.Model.RequestBody.checkOutReqBody;
import com.example.eCommerce.Model.cart;
import com.example.eCommerce.Model.checkOut;
import com.example.eCommerce.Repository.cartRepo;
import com.example.eCommerce.Repository.checkOutRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.List;

@Service
public class checkOutService {

    @Autowired
    private checkOutRepo repo;

    //All Checkout
    public List<checkOut> listAll(){
        return repo.findAll();
    }

    public checkOut save(checkOutReqBody checkOutReq) {
        checkOut check_out = new checkOut();
        check_out.setCheckOutAmount(checkOutReq.getCheckOutAmount());
        check_out.setCheckOutDate(checkOutReq.getCheckOutDate());
        check_out.setUserAccount_userId(checkOutReq.getUserAccount_userId());
        check_out.setUserCards_cardId(checkOutReq.getUserCards_cardId());
        check_out.setCart_cartId(checkOutReq.getCart_cartId());
        return repo.save(check_out);
    }
}
