package com.example.eCommerce.Model;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "checkOutProducts")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class checkOutProducts implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int checkOutProductId;
    @Column
    private int products_productId;
    @Column
    private int checkOut_checkOutId;
}
