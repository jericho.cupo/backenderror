package com.example.eCommerce.SecurityResponses;

public class LoginResponse {
    private String token;

    public void setToken(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }
}
