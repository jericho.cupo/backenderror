package com.example.eCommerce.Services;

import com.example.eCommerce.Model.RequestBody.cartReqBody;
import com.example.eCommerce.Model.RequestBody.productReqBody;
import com.example.eCommerce.Model.cart;
import com.example.eCommerce.Model.product;
import com.example.eCommerce.Repository.cartRepo;
import com.example.eCommerce.Repository.productRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class cartService {

    @Autowired
    private cartRepo repo;


    //All Carts
    public List<cart> listAll(){
        return repo.findAll();
    }

    public cart save(cartReqBody cartReqBody) {
        cart cart1 = new cart();
        cart1.setOrderQuantity(cartReqBody.getOrderQuantity());
//        cart1.setUserAccount_userId(cartReqBody.getUserAccount_userId());
        return repo.save(cart1);
    }

}
