package com.example.eCommerce.Model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import org.springframework.core.serializer.Serializer;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

import static javax.persistence.CascadeType.REMOVE;

@Data
@Entity
@Table(name = "cart_has_products")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class cartHasProducts implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column()
    private int cart_has_productsId;
    @Column
    private int cart_cartId;
    @Column
    private int products_productId;



}
