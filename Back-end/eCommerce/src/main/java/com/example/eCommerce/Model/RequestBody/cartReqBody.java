package com.example.eCommerce.Model.RequestBody;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class cartReqBody {

    private int cartId;
    private int orderQuantity;
    private int userAccount_userId;

}
