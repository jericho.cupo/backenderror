package com.example.eCommerce.Controller;


import com.example.eCommerce.Model.RequestBody.cartReqBody;
import com.example.eCommerce.Model.RequestBody.productReqBody;
import com.example.eCommerce.Model.ResponseBody.cartResBody;
import com.example.eCommerce.Model.ResponseBody.productResBody;
import com.example.eCommerce.Model.cart;
import com.example.eCommerce.Model.product;
import com.example.eCommerce.Services.cartService;
import com.example.eCommerce.Services.productService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class cartController {

    @Autowired
    private cartService cart_service;


    //All Carts
    @GetMapping("/carts")
    public List<cart> list(){
        return cart_service.listAll();
    }


    //adding cart for user
    @PostMapping("/carts/addCart")
    public ResponseEntity<cartResBody> saveCart(@RequestBody cartReqBody cart1){
        cart cart2 = cart_service.save(cart1);
        return new ResponseEntity<cartResBody>(new cartResBody("Successfully save cart.",cart2), HttpStatus.OK);
    }
}
