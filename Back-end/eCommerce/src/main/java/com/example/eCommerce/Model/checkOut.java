package com.example.eCommerce.Model;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;
import java.util.List;

import static javax.persistence.CascadeType.REMOVE;

@Data
@Entity
@Table(name = "checkOut")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class checkOut implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int checkOutId;
    @Column
    private Double checkOutAmount;
    @Column
    private Date checkOutDate;
    @Column
    private int userAccount_userId;
    @Column
    private  int userCards_cardId;
    @Column
    private  int cart_cartId;


    @OneToMany(fetch = FetchType.LAZY, orphanRemoval = false, cascade = REMOVE, mappedBy = "checkOut_checkOutId")
    private List<checkOutProducts> checkOutProducts;
}
