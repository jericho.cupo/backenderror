package com.example.eCommerce.Services.cards;

import com.example.eCommerce.Model.userCard;
import com.example.eCommerce.Repository.userCardsRepo;
import com.sun.istack.FinalArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class userCardsServiceImp implements userCardsService {
    @Autowired
    private userCardsRepo userCardsRepo;

    @Override
    public userCard addCard(userCard newCard) {
        return userCardsRepo.save(newCard);
    }

    @Override
    public userCard findById(int id) {
        return userCardsRepo.findById(id).get();
    }
}
