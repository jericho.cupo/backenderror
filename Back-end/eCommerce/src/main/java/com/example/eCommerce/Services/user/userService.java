package com.example.eCommerce.Services.user;

import com.example.eCommerce.Model.userAccount;

import java.util.List;

public interface userService {


    userAccount addUser(userAccount user);
    List<userAccount> allUsers();
//    List<userAccount> findByCreds(String username, String password);
}
