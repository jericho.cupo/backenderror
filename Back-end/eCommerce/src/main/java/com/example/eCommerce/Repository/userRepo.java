package com.example.eCommerce.Repository;

import com.example.eCommerce.Model.userAccount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface userRepo extends JpaRepository<userAccount, Long> {

    userRepo findByUsername(String userName);

//    @Query(value = "SELECT * FROM user_account u where u.username = :username and password = :password" ,nativeQuery = true)
//    public List<userAccount> findByCreds(@Param("username") String username, @Param("password") String password);
}
