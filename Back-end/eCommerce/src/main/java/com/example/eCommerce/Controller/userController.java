package com.example.eCommerce.Controller;

import com.example.eCommerce.Model.userAccount;
import com.example.eCommerce.Services.user.userService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
public class userController {

    private HttpHeaders headers;

    @Autowired
    userService userService;

    @Autowired
    private PasswordEncoder passwordEncoder;


// default
    @GetMapping("/")
    public String defaultView(){
        return "heelllloooooo";
    }

    //get all users
    @GetMapping("/user")
    public List<userAccount> allUsers(){
        return userService.allUsers();
    }

    //Log in
//    @GetMapping("/login")
//    public List<userAccount> findByCreds(@RequestBody userAccount userCreds){
////        userAccount user = new userAccount();
////        user.setUsername(userCreds.getUsername());
////        user.setPassword(userCreds.getPassword());
//        System.out.println("Username: "+userCreds.getUsername()+"\n"+"Password: "+userCreds.getPassword());
//
////        return userRepo.findByCreds(userCreds.getUsername(),userCreds.getPassword());
//        return userService.findByCreds(userCreds.getUsername(),userCreds.getPassword());
//    }
    //add user
    @PostMapping("/user")
    public ResponseEntity<userAccount> addUser(@RequestBody userAccount newUser){
        userAccount user = new userAccount();

        try {
            user.setFirstName(newUser.getFirstName());
            user.setMiddleName(newUser.getMiddleName());
            user.setLastName(newUser.getLastName());
            user.setGender(newUser.getGender());
            user.setContactNumber(newUser.getContactNumber());
            user.setAddress(newUser.getAddress());
            user.setUsername(newUser.getUsername());
            user.setPassword(passwordEncoder.encode(newUser.getPassword()));
            user.setAccountType(newUser.getAccountType());

            System.out.println(user.toString());
            userService.addUser(user);
            return ResponseEntity.ok().headers(headers).body(user);

        }catch (Exception e){
            System.out.println(e.getMessage());
            return new ResponseEntity<userAccount>(
                    headers,
                    HttpStatus.INTERNAL_SERVER_ERROR
            );
        }
    }
}
