package com.example.eCommerce.Model.ResponseBody;

import lombok.Data;

@Data
public class cartResBody {

    private String message;
    private Object payload;

    public cartResBody(String message, Object payload) {
        this.message = message;
        this.payload = payload;
    }
}
