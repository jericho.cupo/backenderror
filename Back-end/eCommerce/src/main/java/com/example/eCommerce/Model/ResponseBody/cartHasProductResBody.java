package com.example.eCommerce.Model.ResponseBody;


import lombok.Data;

@Data
public class cartHasProductResBody {
    private String message;
    private Object payload;

    public cartHasProductResBody(String message, Object payload) {
        this.message = message;
        this.payload = payload;
    }

}
