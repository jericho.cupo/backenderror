package com.example.eCommerce.Controller;

import com.example.eCommerce.Model.userAccount;
import com.example.eCommerce.Model.userCard;
import com.example.eCommerce.Repository.userCardsRepo;
import com.example.eCommerce.Services.cards.userCardsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
public class userCardController {

    private HttpHeaders headers;

    @Autowired
    userCardsService userCardsService;

    //add card
    @PostMapping("/userCard")
    public ResponseEntity<userCard> addCard(@RequestBody userCard newCard){

        userCard card = new userCard();
        card.setCardName(newCard.getCardName());
        card.setCardNumber(newCard.getCardNumber());
        card.setCardBalance(newCard.getCardBalance());
        card.setCardLimit(newCard.getCardLimit());
        card.setCardType(newCard.getCardType());
        card.setUserAccount_userId(newCard.getUserAccount_userId());

        try {
            userCardsService.addCard(card);
            return ResponseEntity.ok().headers(headers).body(card);

        }catch (Exception e){
            System.out.println(e.getMessage());
            return new ResponseEntity<userCard>(
                    headers,
                    HttpStatus.INTERNAL_SERVER_ERROR
            );
        }
    }

    //get card by id
    //sample url (http://localhost:8080/userCard?id=1)
    @GetMapping("/userCard")
    public userCard findByID(@RequestParam int id){
        return userCardsService.findById(id);
    }

    //update card
    @PutMapping("/userCard")
    public ResponseEntity<userCard> updateCard(@RequestParam int id, @RequestBody userCard updatedCard){
        userCard card = new userCard();
        card.setCardId(id);
        card.setCardName(updatedCard.getCardName());
        card.setCardNumber(updatedCard.getCardNumber());
        card.setCardBalance(updatedCard.getCardBalance());
        card.setCardLimit(updatedCard.getCardLimit());
        card.setCardType(updatedCard.getCardType());
        card.setUserAccount_userId(updatedCard.getUserAccount_userId());

        try{

            userCardsService.addCard(card);
            return ResponseEntity.ok().headers(headers).body(card);

        }catch (Exception e){
            System.out.println(e.getMessage());
            return new ResponseEntity<userCard>(
                    headers,
                    HttpStatus.INTERNAL_SERVER_ERROR
            );
        }

    }
}
