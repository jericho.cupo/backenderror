import React, { useState, useEffect } from 'react';
import { getAllProducts } from '../../Services/Services';
import './product.css';
import { ImEnlarge, ImCart } from "react-icons/im";
import { BsFillBagFill } from "react-icons/bs";
import Pagination from '../Pagination/Pagination';

function Product() {
  
  const [products, setProduct] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [postsPerPage] = useState(5);

  useEffect(()=>{
    getAllProducts().then((response) => {
      setProduct(response.data)
      console.log(response.data);
  })
  .catch(error => {
    console.log(error);
    // localStorage.clear();
    // navigate('/unauthorized')
})
}, [])

// Get current posts
const indexOfLastPost = currentPage * postsPerPage;
const indexOfFirstPost = indexOfLastPost - postsPerPage;
const currentPosts = products.slice(indexOfFirstPost, indexOfLastPost);

const paginate = pageNumber => setCurrentPage(pageNumber);

const renderProducts =(items,index) =>{
  return (
    <div className='bc22-product-container'>
     
      <div className='bc22-product'>
      
        <span className='bc22-circle-icon' style={{float:'left'}}><a href='/' ><ImEnlarge/></a></span>
        <span className='bc22-circle-icon' style={{float:'right'}}><a href='/' ><BsFillBagFill/></a></span>
        <div className='bc22-product-image-container'><img src={items.image} width="45%" height="90%"></img></div>
        <div className='bc22-product-name-container'>
          <h3 className='bc22-product-name'>{items.title}</h3>
          <p className='bc22-product-price'>₱ {items.price}</p>
        </div>
      </div>
    </div>
      )};
  
  return <div className='App' style={{width:'90%', margin:'auto'}}>
  <div className='bc22-banner'></div>
  

    <center>
    
  <div className='bc22-container'>
  <div className='bc22-shopNav'>
    <span className='bc22-title'>SHOP</span>
    <span className='bc22-test123'>Showing {indexOfFirstPost + 1}-{indexOfLastPost} of {products.length} Items</span>
  </div>
      {currentPosts.map(renderProducts)}
      <Pagination
        postsPerPage={postsPerPage}
        totalPosts={products.length}
        paginate={paginate}
      />
  </div>
  </center>
      </div>
    };

export default Product;
