import React from 'react';
import './navBar.css';

function navBar() {
  return (
  <div>
    <div className='bc22-navbar'>
    <a className='bc22-navbar-link' href='/home'>HOME</a>
    <a className='bc22-navbar-link' href='/login'>LOGIN</a>
    <a className='bc22-navbar-link' href='/signup'>SIGNUP</a>
    <a className='bc22-navbar-link' href='/cart'>CART</a>
    <a className='bc22-navbar-link' href='/checkout'>CHECKOUT</a>
    <a className='bc22-navbar-link' href='/product'>PRODUCT</a>
    <a className='bc22-navbar-link' href='/admin'>ADMIN</a>
    <a className='bc22-navbar-link' href='/user'>USER</a>
    <a className='bc22-navbar-link' href='/index'>LOGOUT</a>
    </div>
  </div>
  )
};

export default navBar;
