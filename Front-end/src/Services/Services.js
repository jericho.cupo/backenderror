import axios from 'axios';

//GET ALL PRODUCTS
export const getAllProducts=()=>{
    return axios({
        method:'GET',
        url:`https://fakestoreapi.com/products`,
    })
}


