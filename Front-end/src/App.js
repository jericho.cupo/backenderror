import './App.css';
import {BrowserRouter, Route, Routes} from "react-router-dom";
import NavBar from "./Components/NavigationBar/navBar";
import Login from "./Components/Login/login";
import Signup from "./Components/Signup/signup";
import Home from "./Components/Home/home";
import Product from "./Components/Product/product";
import Cart from "./Components/Cart/cart";
import Checkout from "./Components/Checkout/checkout";
import User from "./Components/User/user";
import Admin from "./Components/Admin/admin";
import LandingPage from "./Components/LandingPage/landingPage";



function App() {
  return( 
  
    <div className='App'>
    <NavBar/>
      <BrowserRouter>
      <Routes>
      <Route path = '/index' element = {<Home/>}></Route>
        <Route path = '/home' element = {<Home/>}></Route>
        <Route path = '/login' element = {<Login/>}></Route>
        <Route path = '/signup' element = {<Signup/>}></Route>
        <Route path = '/product' element = {<Product/>}></Route>
        <Route path = '/cart' element = {<Cart/>}></Route>
        <Route path = '/checkout' element = {<Checkout/>}></Route>
        <Route path = '/user' element = {<User/>}></Route>
        <Route path = '/admin' element = {<Admin/>}></Route>
        <Route path = '*' element = {<LandingPage/>}></Route>
        </Routes>
      </BrowserRouter>
    </div>

);}

export default App;
